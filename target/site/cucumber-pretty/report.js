$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("./features/CucumberGroupsOr Tags.feature");
formatter.feature({
  "name": "Youtube channel name validation",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@CucumberTags"
    }
  ]
});
formatter.scenario({
  "name": "Youtube channel name validations",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@CucumberTags"
    },
    {
      "name": "@Module1"
    },
    {
      "name": "@RegressionTesting"
    },
    {
      "name": "@SmokeTesting"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "Open Chrome browser with URL",
  "keyword": "Given "
});
formatter.match({
  "location": "YoutubeChannelValidationsStepDef.open_Chrome_browser_with_URL()"
});
formatter.result({
  "error_message": "java.lang.NullPointerException\r\n\tat com.testautomation.Listeners.ExtentReportListener.captureScreenShot(ExtentReportListener.java:74)\r\n\tat com.testautomation.Listeners.ExtentReportListener.testStepHandle(ExtentReportListener.java:53)\r\n\tat com.testautomation.StepDef.YoutubeChannelValidationsStepDef.open_Chrome_browser_with_URL(YoutubeChannelValidationsStepDef.java:48)\r\n\tat ✽.Open Chrome browser with URL(./features/CucumberGroupsOr Tags.feature:6)\r\n",
  "status": "failed"
});
formatter.step({
  "name": "Search selenium tutorial",
  "keyword": "When "
});
formatter.match({
  "location": "YoutubeChannelValidationsStepDef.search_selenium_tutorial()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Click on channel name",
  "keyword": "And "
});
formatter.match({
  "location": "YoutubeChannelValidationsStepDef.click_on_channel_name()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Validate channel name",
  "keyword": "Then "
});
formatter.match({
  "location": "YoutubeChannelValidationsStepDef.validate_channel_name()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "Youtube channel name validations",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@CucumberTags"
    },
    {
      "name": "@Module1"
    },
    {
      "name": "@RegressionTesting"
    },
    {
      "name": "@SmokeTesting"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "Open Chrome browser with URL",
  "keyword": "Given "
});
formatter.match({
  "location": "YoutubeChannelValidationsStepDef.open_Chrome_browser_with_URL()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Search selenium tutorial",
  "keyword": "When "
});
formatter.match({
  "location": "YoutubeChannelValidationsStepDef.search_selenium_tutorial()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Click on channel name",
  "keyword": "And "
});
formatter.match({
  "location": "YoutubeChannelValidationsStepDef.click_on_channel_name()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Validate channel name",
  "keyword": "Then "
});
formatter.match({
  "location": "YoutubeChannelValidationsStepDef.validate_channel_name()"
});
formatter.result({
  "error_message": "org.openqa.selenium.NoSuchSessionException: Session ID is null. Using WebDriver after calling quit()?\nBuild info: version: \u00273.11.0\u0027, revision: \u0027e59cfb3\u0027, time: \u00272018-03-11T20:26:55.152Z\u0027\nSystem info: host: \u0027INL-G4W5T72\u0027, ip: \u0027192.168.0.103\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_101\u0027\nDriver info: driver.version: RemoteWebDriver\r\n\tat org.openqa.selenium.remote.HttpCommandExecutor.execute(HttpCommandExecutor.java:125)\r\n\tat org.openqa.selenium.remote.service.DriverCommandExecutor.execute(DriverCommandExecutor.java:83)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:545)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:602)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.getScreenshotAs(RemoteWebDriver.java:291)\r\n\tat com.testautomation.Listeners.ExtentReportListener.captureScreenShot(ExtentReportListener.java:74)\r\n\tat com.testautomation.Listeners.ExtentReportListener.testStepHandle(ExtentReportListener.java:53)\r\n\tat com.testautomation.StepDef.YoutubeChannelValidationsStepDef.validate_channel_name(YoutubeChannelValidationsStepDef.java:109)\r\n\tat ✽.Validate channel name(./features/CucumberGroupsOr Tags.feature:23)\r\n",
  "status": "failed"
});
formatter.after({
  "status": "passed"
});
});